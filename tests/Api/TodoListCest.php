<?php
namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function _before(ApiTester $I)
    {
    }

    public function iShouldFetchData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('http://localhost/todo-application/Exercise9/API.php'); 
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson(); 
        $I->seeResponseContainsJson(['status' => 'success']);

    }

    public function iShouldInsertData(ApiTester $I)
    {
        $id = 5;
        $I->haveHttpHeader('Content-Type', 'application/json');
        $data = ['id' => $id, 'task_name' => 'inside', 'task_title' => 'out'];
        $I->sendPOST('http://localhost/todo-application/Exercise9/API.php', $data);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();   
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldUpdateData(ApiTester $I)
    {   
        $id = 2;
        $I->haveHttpHeader('Content-Type', 'application/json');
        $data = ['id' => $id, 'task_name' => 'asdasdas', 'task_title' => 'wwww'];
        $json_data = json_encode($data);
        $I->sendPUT('http://localhost/todo-application/Exercise9/API.php/' . $id, $json_data);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();   
        $I->seeResponseContainsJson(['status' => 'success']);
    }
    
    

    public function iShouldDeleteData(ApiTester $I)
    {
        $ids = 5;
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('http://localhost/todo-application/Exercise9/API.php/' . $ids);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }
}
