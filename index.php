<?php
class API {
    public function fullName($name) {
        echo "Full Name:" . $name;
    }

    public function Hobbies($hobbies) {
        echo "Hobbies: <br>";
        foreach ($hobbies as $hobby) {
            echo "-" . $hobby;
        }
    }

    public function printPersonalInfo($info) {
        echo "Age: " . $info->age;
        echo "<br> Email: " . $info->email;
        echo "Birthday: " . $info->birthday;
    }
}

$api = new API();
$api->fullName("Aldous Lambert Noel M. Omictin <br>");
$api->Hobbies(["Playing games <br>", "Watching movies <br>", "Dancing <br>"]);

$personalInfo = new stdClass();
$personalInfo->age = 21;
$personalInfo->email = "alnmomictin@addu.edu.ph <br>";
$personalInfo->birthday = "April 11, 2003";

$api->printPersonalInfo($personalInfo);
?>
