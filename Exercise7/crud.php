<?php
$host = "localhost";
$username = "root";
$password = "";
$database = "p8_exercise_backend";

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['create'])) {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $middle_name = $_POST['middle_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
    VALUES ('$first_name', '$last_name', '$middle_name', '$birthday', '$address')";
    
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully <br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Read
$sql = "SELECT * FROM employee";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "ID: " . $row["id"]. " - Name: " . $row["first_name"]. " " . $row["middle_name"]. " " . $row["last_name"]. " - Birthday: " . $row["birthday"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
}

// Update
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['update'])) {
    $update_id = $_POST['update_id'];
    $new_address = $_POST['new_address'];

    $sql = "UPDATE employee SET address = '$new_address' WHERE id = $update_id";
    
    if ($conn->query($sql) === TRUE) {
        echo "Address updated successfully for employee with ID $update_id <br>";
    } else {
        echo "Error updating address: " . $conn->error;
    }
}


// Delete
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete'])) {
    $delete_id = $_POST['delete_id'];

    $sql = "DELETE FROM employee WHERE id = $delete_id";
    
    if ($conn->query($sql) === TRUE) {
        echo "Employee with ID $delete_id deleted successfully <br>";
    } else {
        echo "Error deleting employee: " . $conn->error;
    }
}

$conn->close();
?>