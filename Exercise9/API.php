<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once ('MysqliDb.php');
$request_method = $_SERVER['REQUEST_METHOD'];
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    http_response_code(200);
    exit();
}

class API {
    public $db;
    public function __construct()
    {
        try {
            $this->db = new MysqliDb('localhost', 'root', '', 'employee');
        } catch (Exception $e) {
            die('Database connection failed: ' . $e->getMessage());
        }
    }

    public function httpGet($payload) {
        if (!is_array($payload)) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Payload must be an array'
            ));
            return;
        }
        $result = $this->db->get('tbl_to_do_list'); 
        if ($result) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Failed Fetch Request'
            ));
        }
    }
    
    public function httpPost($payload) {
        if (!is_array($payload) || empty($payload)) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Payload must be a non-empty array'
            ));
            return;
        }

        $insert_id = $this->db->insert('tbl_to_do_list', $payload); 
        if ($insert_id) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'success',
                'data' => $payload
            ));
        } else {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Failed to Insert Data'
            ));
        }
    }

    public function httpPut($id, $payload) {
        if (is_null($id) || empty($id)) {
            echo json_encode([
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Invalid id, non-null and non-empty id expected.'
            ]);
            return;
        }
    
        if (empty($payload)) {
            echo json_encode([
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Invalid payload format, non-empty array expected.'
            ]);
            return;
        }
    
        if (isset($payload['id']) && $payload['id'] != $id) {
            echo json_encode([
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'ID mismatch between the parameter and payload.'
            ]);
            return;
        }
    
        try {
            $this->db->where('id', $id);
            if ($this->db->update('tbl_to_do_list', $payload)) {
                echo json_encode([
                    'method' => 'PUT',
                    'status' => 'success',
                    'data' => $payload
                ]);
            } else {
                echo json_encode([
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Failed to Update Data'
                ]);
            }
        } catch (Exception $e) {
            echo json_encode([
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Failed to Update Data: ' . $this->db->getLastError()
            ]);
        }
    }    

    public function httpDelete($id) {
        if (is_null($id) || empty($id)) {
            echo json_encode([
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Invalid id, non-null and non-empty id expected.'
            ]);
            return;
        }

        try {
            $this->db->where('id', $id);
            if ($this->db->delete('tbl_to_do_list')) {
                echo json_encode([
                    'method' => 'DELETE',
                    'status' => 'success',
                    'data' => ['id' => $id]
                ]);
            } else {
                echo json_encode([
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'Failed to Delete Data'
                ]);
            }
        } catch (Exception $e) {
            echo json_encode([
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Failed to Delete Data: ' . $this->db->getLastError()
            ]);
        }
    }
}

if ($request_method === 'GET') {
    $received_data = $_GET;
} elseif ($request_method === 'PUT' || $request_method === 'DELETE') {
    $url_parts = explode('/', $_SERVER['REQUEST_URI']);
    $id = end($url_parts);
    $json_payload = file_get_contents('php://input');
    $received_data = json_decode($json_payload, true);
} else {
    $json_payload = file_get_contents('php://input');
    $received_data = json_decode($json_payload, true);
}

$api = new API;
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;   
    case 'PUT':
        $api->httpPut($id, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($id);
        break;       
    default:
        echo json_encode(array(
            'method' => $request_method,
            'status' => 'failed',
            'message' => 'Unsupported request method'
        ));
    }
?>