<?php
require_once ('D:\Xampp\htdocs\todo-application\vendor\thingengineer\mysqli-database-class\MysqliDb.php');
$host = "localhost";
$username = "root";
$password = "";
$database = "p8_exercise_backend";

$db = new MysqliDb ($host, $username, $password, $database);
$db->setPrefix ('my_');
$db->autoReconnect = false;

// Create
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['create'])) {
    $data = Array (
        "first_name" => $_POST['first_name'],
        "last_name" => $_POST['last_name'],
        "middle_name" => $_POST['middle_name'],
        "birthday" => $_POST['birthday'],
        "address" => $_POST['address']
    );
    
    $id = $db->insert ('employee', $data);
    if ($id) {
        echo "New record created successfully <br>";
    } else {
        echo "Error: " . $db->getLastError() . "<br>";
    }
}

// Read
$employees = $db->get ('employee');
if ($db->count > 0) {
    foreach ($employees as $employee) {
        echo "ID: " . $employee["id"]. " - Name: " . $employee["first_name"]. " " . $employee["middle_name"]. " " . $employee["last_name"]. " - Birthday: " . $employee["birthday"]. " - Address: " . $employee["address"]. "<br>";
    }
} else {
    echo "0 results";
}

// Update
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['update'])) {
    $update_id = $_POST['update_id'];
    $new_address = $_POST['new_address'];

    $db->where ('id', $update_id);
    $data = Array ("address" => $new_address);
    $db->update ('employee', $data);
    
    if ($db->count > 0) {
        echo "Address updated successfully for employee with ID $update_id <br>";
    } else {
        echo "Error updating address: " . $db->getLastError();
    }
}

// Delete
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete'])) {
    $delete_id = $_POST['delete_id'];

    $db->where ('id', $delete_id);
    $db->delete ('employee');
    
    if ($db->count > 0) {
        echo "Employee with ID $delete_id deleted successfully <br>";
    } else {
        echo "Error deleting employee: " . $db->getLastError();
    }
}
?>